@rem rename from stock names to app-context names

@ren 0.* button_0.*
@ren 1.* button_1.*
@ren 2.* button_2.*
@ren 3.* button_3.*
@ren 4.* button_4.*
@ren 5.* button_5.*
@ren 6.* button_6.*
@ren 7.* button_7.*
@ren 8.* button_8.*
@ren 9.* button_9.*
@ren Synchronize.* mark_sending.*
@ren External.* mark_pending.*
@ren User.* image_user.*
@ren Survey.* button_view_feeding.*
@ren "Chalk Bag".* button_subtract_bag.*
@ren "Minus".* button_subtract.*
@ren "Settings".* button_settings.*
@ren "Fish Food".* button_record_cage.*
@ren "Cancel".* button_mortality.*
@ren "Temperature".* button_measurements.*
@ren "Thermometer".* button_inspection.*
@ren "Rotate Right".* button_logout.*
@ren "Feed In".* button_feeding.*
@ren "Return".* button_back.*
@ren "Attention".* button_alarm.*
@ren "Checked".* button_accept.*
@ren "Plus".* button_add.*
@ren "Backspace".* button_bksp.*
@rem no direct renames
@copy "button_subtract_bag".* button_add_bag.*
@copy "button_1".* button_decimal_point.*
@ren "Collect".* button_decimal_point_dot.*

@rem pause