@echo off
@cd "5 lollipop"
@call ..\makecopy\image_copy ldpi 32 72 32
@call ..\makecopy\image_copy - 48 48 96
@call ..\makecopy\image_copy mdpi 72 144 96 
@call ..\makecopy\image_copy hdpi 72 144 96
@call ..\makecopy\image_copy xhdpi 96 192 144
@call ..\makecopy\image_copy xxhdpi 144 288 192
@call ..\makecopy\image_copy xxxhdpi 192 384 288
@call ..\makecopy\image_copy sw600dp-xhdpi 144 288 192