@echo on
@if "%1"=="/?" goto no_param
@if "%1"=="" goto no_param
@goto :start

:no_param
@echo .
@echo create the appropriate drawable-xxx directory and fill it from other directories avaialble
@echo .
@echo params:
@echo 1 (string): the string I shall attach to "drawable-" in order to produce the correct directory name. "-" (no quotes) for plain "drawable", no addtion
@echo 2 (int): action-bar size
@echo 3 (int): generic buttons size
@echo 4 (int): special buttons size (eg numpad)
@goto :end

:start

if "%1" == "-" goto :no_special_dir
  @set target=drawable-%1
  @goto :next_1
:no_special_dir
  @set target=drawable

:next_1

@md %target%

@rem action bar
if "%2" == "-" goto :end_2

@copy %2\button_logout.* %target%
@copy %2\button_settings.* %target%
@copy %2\mark_pending.* %target%
@copy %2\mark_sending.* %target%
@copy %2\button_back.* %target%

:end_2

@rem generic buttons
if "%3" == "-" goto :end_3

@copy %3\button_view_feeding.* %target%
@copy %3\button_record_cage.* %target%
@copy %3\button_measurements.* %target%
@copy %3\image_user.* %target%
@copy %3\button_subtract_bag.* %target%
@copy %3\button_subtract.* %target%
@copy %3\button_mortality.* %target%
@copy %3\button_inspection.* %target%
@copy %3\button_feeding.* %target%
@copy %3\button_alarm.* %target%
@copy %3\button_accept.* %target%
@copy %3\button_add.* %target%
@copy %3\button_add_bag.* %target%

:end_3

@rem numpad
if "%4" == "-" goto :end_4

@copy %4\button_0.* %target%
@copy %4\button_1.* %target%
@copy %4\button_2.* %target%
@copy %4\button_3.* %target%
@copy %4\button_4.* %target%
@copy %4\button_5.* %target%
@copy %4\button_6.* %target%
@copy %4\button_7.* %target%
@copy %4\button_8.* %target%
@copy %4\button_9.* %target%
@copy %4\button_decimal_point.* %target%
@copy %4\button_bksp.* %target%

:end_4

rem cls
rem echo FIXMEEEEEEE !!!!!!!!
rem del %target%\button_1.png
rem ren %target%\button_decimal_point.png button_1.png

:end
@rem pause

